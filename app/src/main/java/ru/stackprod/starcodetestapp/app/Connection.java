package ru.stackprod.starcodetestapp.app;

import android.app.ProgressDialog;
import android.content.Context;
import android.text.TextUtils;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

/**
 * Created by Руслан on 19.02.2016.
 */
public class Connection {
    Context mContext;
    private RequestQueue mRequestQueue;
    private static Connection mInstance;
    public static final String TAG = Connection.class.getSimpleName();

    public static synchronized Connection getInstance(Context mContext) {
        if (mInstance == null)
            mInstance = new Connection(mContext);
        return mInstance;
    }

    public Connection(Context mContext) {
        this.mContext = mContext;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mContext);
        }
        return mRequestQueue;
    }


    public <T> void addToRequestQueue(com.android.volley.Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

}