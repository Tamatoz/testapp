package ru.stackprod.starcodetestapp.app;

import android.media.AudioManager;
import android.media.MediaPlayer;

/**
 * Created by Руслан on 19.02.2016.
 */
public class Player {
    private static int currentPlayPosition = -1;
    private static MediaPlayer mPlayer;
    public static boolean cancelPlaying = false;

    public static void play(String url){
        cancelPlaying = false;
        mPlayer = new MediaPlayer();
        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        try {
            mPlayer.setDataSource(url);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            mPlayer.prepare();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(!cancelPlaying)
            mPlayer.start();
        else
            currentPlayPosition = -1;
    }

    public static void stop(){
        if(mPlayer!=null && mPlayer.isPlaying()){
            mPlayer.stop();
            currentPlayPosition = -1;
        }
    }

    public static void setCurrentPlayPosition(int currentPlayPosition) {
        Player.currentPlayPosition = currentPlayPosition;
    }



    public static int getCurrentPlayPosition() {
        return currentPlayPosition;
    }

}
