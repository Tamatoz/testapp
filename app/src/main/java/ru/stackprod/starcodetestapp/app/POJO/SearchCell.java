package ru.stackprod.starcodetestapp.app.POJO;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Руслан on 19.02.2016.
 */
public class SearchCell implements Serializable{

    @SerializedName("collectionName")
    private String collectionName;

    @SerializedName("artistName")
    private String artistName;

    @SerializedName("artworkUrl100")
    private String imageURL;

    @SerializedName("previewUrl")
    private String previewUrl;

    public String getCollectionName() {
        return collectionName;
    }

    public String getArtistName() {
        return artistName;
    }

    public String getImageURL() {
        return imageURL;
    }

    public String getPreviewUrl() {
        return previewUrl;
    }
}
