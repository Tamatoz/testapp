package ru.stackprod.starcodetestapp.app;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import ru.stackprod.starcodetestapp.app.POJO.SearchCell;

import java.util.ArrayList;

/**
 * Created by Руслан on 19.02.2016.
 */
public class ResultActivity extends AppCompatActivity {
    SearchListAdapter adapter;
    ArrayList objsList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        ListView resultList = (ListView) findViewById(R.id.results_list);
        objsList = getIntent().getParcelableArrayListExtra("result");
        adapter = new SearchListAdapter(this, R.layout.item_search_list, objsList);
        resultList.setAdapter(adapter);

        resultList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,final int position, long id) {
                startPlaying(position);
            }
        });

        ActionBarCustomizer.restoreActionBar(this, true);
    }

    private void startPlaying(final int position){
        Thread playThread = new Thread(new Runnable() {
            @Override
            public void run() {
                if(position != Player.getCurrentPlayPosition()) {
                    Player.stop();
                    Player.setCurrentPlayPosition(position);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyDataSetChanged();
                        }
                    });
                    Player.play(((SearchCell) objsList.get(position)).getPreviewUrl());
                }
                else
                    Player.stop();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyDataSetChanged();
                        }
                    });
            }
        });
        playThread.start();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                Player.stop();
                Player.cancelPlaying = true;
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
