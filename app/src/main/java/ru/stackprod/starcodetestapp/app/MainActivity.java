package ru.stackprod.starcodetestapp.app;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.android.volley.*;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import ru.stackprod.starcodetestapp.app.POJO.SearchCell;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    EditText searchField;
    Button searchButton;
    ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        searchField = (EditText) findViewById(R.id.search_field);
        searchButton = (Button) findViewById(R.id.search_button);

        dialog = new ProgressDialog(this);
        dialog.setTitle("Загрузка");
        dialog.setMessage("Пожалуйста, подождите...");

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.isEmpty(searchField.getText()))
                    initSearchRequest(searchField.getText().toString());
                else
                    Toast.makeText(MainActivity.this, "Введите поисковый запрос", Toast.LENGTH_SHORT).show();
            }
        });

        ActionBarCustomizer.restoreActionBar(this, false);
    }

    private void initSearchRequest(String request){
        dialog.show();
        String url = "";
        try {
            url = Addresses.SEARCH_URL + URLEncoder.encode(request, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        Gson gson = new Gson();
                        JsonParser parser = new JsonParser();
                        JsonObject jObj = parser.parse(s).getAsJsonObject();
                        JsonArray jArray = jObj.get("results").getAsJsonArray();
                        Type type = new TypeToken<ArrayList<SearchCell>>() {
                        }.getType();
                        ArrayList<SearchCell> list = gson.fromJson(jArray, type);
                        dialog.dismiss();
                        if(list.size() == 0){
                            Toast.makeText(MainActivity.this, "По Вашему запросу ничего не найдено", Toast.LENGTH_SHORT).show();
                        } else {
                            Intent intent = new Intent(MainActivity.this, ResultActivity.class);
                            intent.putExtra("result", list);
                            startActivity(intent);
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.d("search_error", volleyError.toString());
                        Toast.makeText(MainActivity.this, "При загрузке данных произошла ошибка", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(5000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Connection.getInstance(MainActivity.this).addToRequestQueue(stringRequest);
    }


}
