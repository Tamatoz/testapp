package ru.stackprod.starcodetestapp.app;

import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Руслан on 19.02.2016.
 */
public class ActionBarCustomizer {
    public static void restoreActionBar(AppCompatActivity parent, boolean withBackButton){
        ActionBar actionBar = parent.getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(parent.getResources().getColor(R.color.main_color)));
        actionBar.setDisplayHomeAsUpEnabled(withBackButton);
    }
}
