package ru.stackprod.starcodetestapp.app;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import ru.stackprod.starcodetestapp.app.POJO.SearchCell;

import java.util.ArrayList;

/**
 * Created by Руслан on 19.02.2016.
 */
public class SearchListAdapter extends ArrayAdapter {
    ArrayList<SearchCell> objs;
    int resource;
    Context ctx;
    SearchViewHolder currentPlayingCell = null;
    public SearchListAdapter(Context context, int resource, ArrayList<SearchCell> objs) {
        super(context, resource, objs);
        this.objs = objs;
        this.resource = resource;
        this.ctx = context;
    }


    private static class SearchViewHolder{
        TextView artistName, collName;
        ImageView collImage;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SearchViewHolder holder;
        if(convertView == null){
            convertView = View.inflate(ctx, resource, null);
            holder = new SearchViewHolder();
            holder.artistName = (TextView) convertView.findViewById(R.id.artist_name);
            holder.collName = (TextView) convertView.findViewById(R.id.coll_name);
            holder.collImage = (ImageView) convertView.findViewById(R.id.coll_image);
            convertView.setTag(holder);
        } else {
            holder = (SearchViewHolder) convertView.getTag();
        }
        if(Player.getCurrentPlayPosition() == position) {
            convertView.setBackgroundColor(ctx.getResources().getColor(R.color.main_color));
            holder.artistName.setTextColor(ctx.getResources().getColor(android.R.color.white));
            holder.collName.setTextColor(ctx.getResources().getColor(R.color.light_grey));
        } else {
            convertView.setBackgroundColor(ctx.getResources().getColor(android.R.color.white));
            holder.artistName.setTextColor(ctx.getResources().getColor(android.R.color.black));
            holder.collName.setTextColor(ctx.getResources().getColor(R.color.grey));
        }
        holder.artistName.setText(objs.get(position).getArtistName());
        holder.collName.setText(objs.get(position).getCollectionName());
        Picasso.with(ctx).load(objs.get(position).getImageURL()).into(holder.collImage);
        return convertView;
    }
}
